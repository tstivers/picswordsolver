﻿using System;
using System.IO;

namespace PicsWordSolver
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Building dictionary...");
            var dictionaryBuilder = new DictionaryBuilder();
            var dictionary = dictionaryBuilder.BuildDictionary(
                Path.Combine(Directory.GetCurrentDirectory(), "photodata.txt"));

            Console.WriteLine("Enter total number of characters in answer (1-9): ");
            var key = Console.ReadKey();
            var numCharacters = int.Parse(key.KeyChar.ToString());
            Console.WriteLine();

            Console.WriteLine("Enter the available letters and press enter: ");
            var availableCharacters = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine("Finding solutions...");
            var solutionFinder = new SolutionFinder(dictionary);
            var solutions = solutionFinder.FindSolutions(numCharacters, availableCharacters);

            if (solutions.Length == 0)
            {
                Console.WriteLine("No solutions found.");
                return;
            }

            Console.WriteLine("Found solutions:");

            foreach (var solution in solutions)
            {
                Console.WriteLine(solution);
            }

            Console.WriteLine("Press the any key to continue.");
            Console.ReadKey();
        }
    }
}
