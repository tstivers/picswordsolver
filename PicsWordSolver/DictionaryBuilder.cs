﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;

namespace PicsWordSolver
{
    public class DictionaryBuilder
    {
        public string[] BuildDictionary(string dataFile)
        {
            var jsonData = File.ReadAllText(dataFile);

            var data = JsonConvert.DeserializeObject<PhotoData[]>(jsonData);

            return data.Select(photoData => photoData.Solution).ToArray();
        }

        public class PhotoData
        {
            public string Solution { get; set; }
        }
    }
}