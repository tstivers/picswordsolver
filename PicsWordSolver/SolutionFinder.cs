﻿using System;
using System.Collections.Generic;

namespace PicsWordSolver
{
    public class SolutionFinder
    {
        private readonly string[] _dictionary;

        public SolutionFinder(string[] dictionary)
        {
            _dictionary = dictionary;
        }

        public string[] FindSolutions(int numCharacters, string availableCharacters)
        {
            if (numCharacters < 1 || numCharacters > 9)
            {
                var message = $"Parameter `{nameof(numCharacters)}` must be 1-9.";

                throw new ArgumentOutOfRangeException(nameof(numCharacters), message);
            }

            if (string.IsNullOrWhiteSpace(availableCharacters))
            {
                var message = $"Parameter `{nameof(availableCharacters)}` must not be null or whitespace.";

                throw new ArgumentException(message, nameof(availableCharacters));
            }

            var foundSolutions = new List<string>();
            foreach (var solution in _dictionary)
            {
                if (solution.Length != numCharacters) continue;
                if (IsPotentialSolution(solution, availableCharacters))
                {
                    foundSolutions.Add(solution);
                }
            }

            return foundSolutions.Count > 0 ? foundSolutions.ToArray() : null;
        }

        private bool IsPotentialSolution(string solution, string availableCharacters)
        {
            solution = solution.ToUpper();
            availableCharacters = availableCharacters.ToUpper();

            var foundSolution = "";
            foreach (var solutionCharacter in solution)
            {
                var matchFound = false;
                for (var i = 0; i < availableCharacters.Length; i++)
                {
                    if (availableCharacters[i] != solutionCharacter) continue;
                    
                    foundSolution += availableCharacters[i];
                    availableCharacters = availableCharacters.Remove(i, 1);
                    matchFound = true;
                    break;
                }

                if (!matchFound) return false;
            }

            return foundSolution == solution;
        }
    }
}